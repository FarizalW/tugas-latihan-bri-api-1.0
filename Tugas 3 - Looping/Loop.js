/*
Soal No. 1 Looping While
Anda diminta untuk membuat looping dengan while supaya output yang dihasilkan :
LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding
*/
let hiya = 0

while (hiya<=20) {
  text = hiya + ' - ' + 'I love coding';
  hiya += 2;
  console.log(text);
}

// ============================================================================

/*
Soal No.2 Looping menggunakan for
Buatlah looping dengan syarat: 
  - Jika angkanya ganjil maka tampilkan "Semangat"
  - Jika angkanya genap maka tampilkan "Berkualitas"
  - Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan "I Love Coding"
*/

for ( let i = 1 ; i <= 20 ; i++)  {
 let result;
 if (i%3 == 0 && i%2 == 0) {
  result=(i + ' - ' + 'I Love Coding'); 
 } else if (i%2 == 0) {
   result=(i + ' - ' + 'Berkualitas');
 } else if (i%2 == 1) {
  result=(i + ' - ' + 'Semangat');
 }
  console.log(result);
}