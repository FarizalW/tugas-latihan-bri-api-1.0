/*
1. Soal No. 1 (Membuat kalimat)
*/
console.log("\n");
let word = 'JavaScript '; 
let second = 'is '; 
let third = 'awesome '; 
let fourth = 'and '; 
let fifth = 'I '; 
let sixth = 'love '; 
let seventh = 'it! ';

let kalimat = (word + second + third + fourth + fifth + sixth + seventh);
//Buatlah supaya menjadi kalimat "JavaScript is awesome and I love it!"

//Abaikan kode di bawah ini
module.exports = {kalimat};