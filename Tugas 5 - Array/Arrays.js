/* 
 Buatlah sebuah variable dengan nama nomorGenap yang merupakan sebuah array dengan ketentuan: 
  - Array tersebut menampung bilangan genap dari 1 hingga 100

  Note: 
    - Agar lebih mudah bisa menggunakan for loop dan logika if untuk mengisi array tersebut.
*/

function nomorGenap() {
  let arrGenap = [];
  for (i =1; i <= 100; i++) {
    let angka;
    if (i%2 == 0) {
      arrGenap.push(i);
    }
  }
  return arrGenap;
}

//Hiraukan kode di bawah ini
module.exports = { nomorGenap };
