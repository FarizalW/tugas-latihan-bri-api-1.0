let readBooksPromise = require("./promise.js")

let books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
]

/*
Lanjutkan code pada index.js untuk memanggil function readBooksPromise. Buku yang akan dihabiskan adalah buku-buku di dalam array books.
Pertama function readBooksPromise menerima input waktu yang dimiliki yaitu 10000ms (10 detik) dan books pada indeks ke-0.
Setelah mendapatkan callback sisa waktu yang dikirim lewat callback, sisa waktu tersebut dipakai untuk membaca buku pada indeks ke-1.
Begitu seterusnya sampai waktu habis atau semua buku sudah terbaca.
*/
const main = async () => {
  const BukuOne = await readBooksPromise(10000, books).catch((a)=> console.log(a));
  console.log(BukuOne);
  }
  
// Tulis code untuk memanggil function readBooksPromise di sini 

